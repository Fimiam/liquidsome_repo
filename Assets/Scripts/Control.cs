using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
using Random = UnityEngine.Random;

public class Control : MonoBehaviour
{
    public static Control Instance { get; private set; }

    public Cup cup;

    public new MyCamera camera;

    public Liquid liquid;

    public SellPoint sellPoint;

    public UpgradesController upgradesController;

    public Pack controlPack;

    Plane inputPlane;

    private float selectRadius = .2f;

    public List<MergableItemData> colorList = new List<MergableItemData>();

    public int defaultItemsCount = 20;
    public int defaultItemsCost = 20;
    public int itemsCost = 20;
    public int itemsCount = 20;

    public bool soundActive, vibroActive;

    public int gameLaunches;

    public bool addTutorialDone, mergeTutorialDone, sellTutorialDone, expandTutorialDone, buttonTutorialActive;

    public MoveTutorial moveTutorial;

    private Coroutine chekingAfterUpgrade;

    private void Awake()
    {
        Instance = this;

        Application.targetFrameRate = 60;

        soundActive = PlayerPrefs.GetInt("sound", 1) > 0;
        vibroActive = PlayerPrefs.GetInt("vibro", 1) > 0;

        gameLaunches = PlayerPrefs.GetInt("launches");

        gameLaunches++;

        PlayerPrefs.SetInt("launches", gameLaunches);

        addTutorialDone = PlayerPrefs.GetInt("add_tutor") > 0;

        mergeTutorialDone = PlayerPrefs.GetInt("merge_tutor") > 0;

        sellTutorialDone = PlayerPrefs.GetInt("sell_tutor") > 0;

        expandTutorialDone = PlayerPrefs.GetInt("expand_tutor") > 0;
    }

    internal void AddTutorialDone()
    {
        addTutorialDone = true;

        PlayerPrefs.SetInt("add_tutor", 1);

        if (cup.packs.Count > 1)
            moveTutorial.SetMergeMoveTutorial(cup.packs[1], cup.packs[0]);
    }

    internal void MergeTutorialDone()
    {
        mergeTutorialDone = true;

        PlayerPrefs.SetInt("merge_tutor", 1);
    }

    public void SellTutorialDone()
    {
        sellTutorialDone = true;

        PlayerPrefs.SetInt("sell_tutor", 1);
    }

    public void ExpandTutorialDone()
    {
        expandTutorialDone = true;

        PlayerPrefs.SetInt("expand_tutor", 1);

        buttonTutorialActive = false;
    }

    private void Start()
    {
        inputPlane = new Plane(Vector3.back, Vector3.zero);
    }

    internal void SellingPack(Pack pack)
    {
        if(!sellTutorialDone)
        {
            SellTutorialDone();

            moveTutorial.StopMoveTutorial();
        }

        VibrationManager.instance.SellingVibro();
    }

    public Color GetColorById(int num)
    {
        var data = colorList.Find(c => c.id == num);

        if (data == null) data = colorList[0];

        return data.color;
    }

    internal void SetSound(bool v)
    {
        soundActive = v;

        PlayerPrefs.SetInt("sound", soundActive ? 1 : 0);
    }

    internal void SetVibro(bool v)
    {
        vibroActive = v;

        PlayerPrefs.SetInt("vibro", vibroActive ? 1 : 0);
    }

    private void Update()
    {
        if(controlPack == null)
        {
            if(Input.GetMouseButtonDown(0) & !IsPointerOverUIObject() & addTutorialDone & !buttonTutorialActive)
            {
                var point = GetInputPoint();

                var colls = Physics2D.OverlapCircleAll(point, selectRadius);
                
                if(colls.Length > 0)
                {
                    foreach (var coll in colls)
                    {
                        if(coll.TryGetComponent<Blob>(out var blob))
                        {
                            controlPack = blob.pack;

                            controlPack.SetHold(true);

                            break;
                        }
                    }

                    if(!mergeTutorialDone || !sellTutorialDone)
                    {
                        moveTutorial.StopMoveTutorial();
                    }
                }
                else
                {

                }

            }
        }
        else
        {
            if(!Input.GetMouseButton(0))
            {
                controlPack.SetHold(false);

                controlPack = null;

                if(!mergeTutorialDone)
                {
                    if (cup.packs.Count > 1)
                        moveTutorial.SetMergeMoveTutorial(cup.packs[1], cup.packs[0]);
                }

                if(!sellTutorialDone)
                {
                    var packs = cup.GetPacksForSell();

                    if(packs.Count > 0)
                    {
                        moveTutorial.SetSellMoveTutorial(packs[0], sellPoint.sellPoint.position);
                    }
                }
            }
        }

    }

    public Vector2 GetInputPoint()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float enter = 0f;

        inputPlane.Raycast(ray, out enter);

        return ray.GetPoint(enter);
    }

    public bool IsPointerOverUIObject()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        return EventSystem.current.IsPointerOverGameObject();
#else
        if (Input.touchCount < 1) return false;

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
#endif

    }

    internal void MergePacks(Pack pack1, Pack pack2, Vector2 position)
    {
        if(!mergeTutorialDone)
        {
            moveTutorial.StopMoveTutorial();

            MergeTutorialDone();
        }

        pack1.inMerge = true;
        pack2.inMerge = true;

        int countToMerge = pack1.blobs.Count < pack2.blobs.Count ? pack1.blobs.Count : pack2.blobs.Count;

        pack1.blobs.OrderBy(f => Vector2.SqrMagnitude(f.body.position - position));
        pack2.blobs.OrderBy(f => Vector2.SqrMagnitude(f.body.position - position));

        List<PairToMerge> pairs = new List<PairToMerge>();

        List<Blob> left = new List<Blob>();

        for (int i = 0; i < countToMerge; i++)
        {
            var pair = new PairToMerge();

            pair.blob1 = pack1.blobs[0];
            pair.blob2 = pack2.blobs[0];

            pairs.Add(pair);

            pack1.RemoveBlobAt(0);
            pack2.RemoveBlobAt(0);
        }

        if(pack1.blobs.Count > 0)
        {
            foreach (var b in pack1.blobs)
            {
                left.Add(b);
            }

            pack1.blobs.Clear();
        }

        if(pack2.blobs.Count > 0)
        {
            foreach (var b in pack2.blobs)
            {
                left.Add(b);
            }

            pack2.blobs.Clear();
        }

        pack1.midlePoint = position;
        pack2.midlePoint = position;

        StartCoroutine(Merging(pairs, left));
    }

    private IEnumerator Merging(List<PairToMerge> pairs, List<Blob> left)
    {
        var newPack = pairs[0].blob1.pack;
        var packToDisable = pairs[0].blob2.pack;

        var toMerge = pairs[0].blob1.pack.mergableData.mergeToData;

        newPack.SetupData(toMerge);

        int resultCount = newPack.mergableData.mergedCount;

        int toDestroy = pairs.Count * 2 - resultCount;

        bool destroySwitch = false;

        //Debug.Log("merge Count - " +resultCount);
        //Debug.Log("to destroy count - " + toDestroy);

        for (int i = 0; i < pairs.Count; i++)
        {
            if(destroySwitch)
            {
                //var blob = pairs[i].blob1;

                newPack.AddBlob(pairs[i].blob1);

                //pairs[i].blob2.gameObject.SetActive(false);
                if(toDestroy > 0)
                {
                    toDestroy--;

                    Destroy(pairs[i].blob2.gameObject);
                }
                else
                {
                    newPack.AddBlob(pairs[i].blob2);
                }

                VibrationManager.instance.VibroMerge();
            }
            else
            {
                newPack.AddBlob(pairs[i].blob2);

                //pairs[i].blob1.gameObject.SetActive(false);

                if (toDestroy > 0)
                {
                    toDestroy--;

                    Destroy(pairs[i].blob1.gameObject);
                }
                else
                {
                    newPack.AddBlob(pairs[i].blob1);
                }

                VibrationManager.instance.VibroMerge();
            }

            destroySwitch = !destroySwitch;

            //if(destroySwitch)
                yield return null;
        }

        //packToDisable.gameObject.SetActive(false);

        cup.RemovePack(packToDisable);
        cup.RemovePersist(packToDisable.id);
        cup.RemovePersist(packToDisable.id);
        cup.AddPersist(newPack.id);

        Destroy(packToDisable.gameObject);

        newPack.inMerge = false;

        if (!sellTutorialDone)
        {
            var packs = cup.GetPacksForSell();

            if (packs.Count > 0)
            {
                moveTutorial.SetSellMoveTutorial(packs[0], sellPoint.sellPoint.position);
            }
        }
    }

    public void AddPackClick()
    {
        int toSpawn = 0;

        bool havePacksToSell = cup.HavePacksToSell();

        bool havePacksToMerge = cup.HavePacksToMerge();

        bool noMoneyForNext = Money.Instance.softValue < itemsCost & !havePacksToSell & !havePacksToMerge;

        bool noSpaceForNext = false;

        if (!noMoneyForNext) noSpaceForNext = cup.current + itemsCount >= cup.max;

        if (noMoneyForNext | noSpaceForNext)
        {
            foreach (var item in cup.packs)
            {
                var id = item.id;

                if(id < 6)
                {
                    toSpawn = id - 1;

                    break;
                }
            }
        }
        else
        {
            toSpawn = Random.Range(0, 5);
        }

        cup.SpawnPack(colorList[toSpawn]);

        //doublespawn upgrade

        if(upgradesController.doublespawnUpgrade > 0)
        {
            var rnd = Random.value;

            var spawn = rnd < upgradesController.GetDoublespawnChance();

            if(spawn)
            {
                toSpawn = Random.Range(0, 5);

                cup.SpawnPack(colorList[toSpawn]);
            }
        }
    }

    public void MadeUpgrade()
    {
        if(chekingAfterUpgrade == null)
        {
            chekingAfterUpgrade = StartCoroutine(CheckAfterUpgrade());
        }
    }

    internal void CupUpgraded()
    {
        camera.CupUpgraded();

        liquid.CupUpgraded();

        MadeUpgrade();
    }


    private IEnumerator CheckAfterUpgrade()
    {
        while(Money.Instance.softValue < itemsCost & !cup.HavePacksToSell() & !cup.HavePacksToMerge())
        {
           yield return new WaitForSeconds(1f);

            int toSpawn = 0;

            foreach (var item in cup.packs)
            {
                var id = item.id;

                if (id < 6)
                {
                    toSpawn = id - 1;

                    break;
                }
            }

            cup.SpawnPack(colorList[toSpawn]);
        }

        chekingAfterUpgrade = null;
    }

    public string GetShrinkNumberString(int value)
    {
        if(value < 1000) return value.ToString();

        if(value < 1000000)
        {
            var stringVal = "";

            var thousands = value / 1000;

            stringVal += thousands.ToString();

            value -= thousands * 1000;

            if(value > 100)
            {
                var hundreds = value / 100;

                stringVal += "." + hundreds.ToString();
            }

            stringVal += "K";

            return stringVal;
        }

        return value.ToString();
    }
}


public class PairToMerge
{
    public Blob blob1;
    public Blob blob2;
}

[System.Serializable]
public class NumColor
{
    public MergableItemData data;
}