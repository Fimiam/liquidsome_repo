using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using TMPro;

public class SellPoint : MonoBehaviour
{
    public Transform sellPoint;

    public float sqrDistToSell = .33f;

    Dictionary<Pack, MoneyAdd> activeSales = new Dictionary<Pack, MoneyAdd>();

    //private Pack packForSell;

    public TextMeshPro suggestPriceText;

    private List<Blob> blobsInRange = new List<Blob>();

    public SellSuggest suggest;

    private UpgradesController upgradesController;

    private void Start()
    {
        upgradesController = Control.Instance.upgradesController;
    }

    private void Update()
    {

        if(blobsInRange.Count < 1)
        {
            enabled = false;
        }
        else
        {
            if(Input.GetMouseButtonUp(0) & !blobsInRange[0].pack.inMerge & !blobsInRange[0].pack.inSell)
            {
                StartCoroutine(Selling(blobsInRange[0].pack));

                blobsInRange.Clear();
            }
        }

    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.TryGetComponent<Blob>(out var blob))
        {
            if(blob.pack == Control.Instance.controlPack & !blob.pack.inSell & blob.pack.mergableData.softValue > 0)
            {

                if(blobsInRange.Count == 0 & suggest.blobsInRange[0].pack == blob.pack)
                {
                    blobsInRange.Add(blob);

                    suggest.SellReady();

                    enabled = true;
                }
                else
                {
                    if (blobsInRange[0].pack == blob.pack)
                    {
                        blobsInRange.Add(blob);
                    }
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.TryGetComponent<Blob>(out var blob))
        {
            blobsInRange.Remove(blob);
        }
    }

    private IEnumerator Selling(Pack pack)
    {
        pack.BeginSell();

        var softVal = pack.GetSoftValue();
        var hardVal = pack.GetHardValue();

        //profit upgrade

        if (upgradesController.profitUpgrade > 0)
        {
            var rnd = Random.value;

            var profit = rnd < upgradesController.GetProfitChance();

            if (profit)
            {
                softVal *= 2;
                hardVal *= 2;

                suggest.suggestValue.SetupSuggest(softVal, hardVal);
            }
        }

        //profit upgrade

        suggest.Sold();

        Money.Instance.AddSoft(softVal);
        Money.Instance.AddHard(hardVal);

        Money.Instance.SaveState();

        //var moneyOffset = -sellPoint.right * 2f + (Vector3)Random.insideUnitCircle;

        //MoneyAdd moneyAdd = Money.Instance.AddMoney(sellPoint.TransformPoint(sellPoint.localPosition + moneyOffset));

        //moneyAdd.pendingSales = pack.blobs.Count;

        //activeSales.Add(pack, moneyAdd);

        pack.midlePoint = (Vector2)sellPoint.position;

        List<Blob> toRemove = new List<Blob>();

        while(pack.blobs.Count > 0)
        {
            foreach(var blob in pack.blobs)
            {
                var dist = (blob.body.position - (Vector2)sellPoint.position).sqrMagnitude;

                if(dist < sqrDistToSell)
                {
                    blob.Sell(1, this);

                    toRemove.Add(blob);
                }
            }

            foreach(var blob in toRemove)
            {
                pack.blobs.Remove(blob);
            }

            toRemove.Clear();

            yield return null;
        }
    }

    internal void Sold(Blob blob)
    {
        //MoneyAdd money = null;

        //if(activeSales.TryGetValue(blob.pack, out money))
        //{
        //    money.AddValue(blob.pack.mergableData.softValue);

        //    money.pendingSales--;

        //    if(money.pendingSales <= 0)
        //    {
        //        money.SoldOut();

        //        activeSales.Remove(blob.pack);

        //        blob.pack.Sold();
        //    }
        //}
    }
}
