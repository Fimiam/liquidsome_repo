using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using DG.Tweening;
using UnityEngine.UI;

public class MoneyAdd : MonoBehaviour
{
    public TextMeshProUGUI text;

    public RectTransform imageRectTransform;

    public int pendingSales;

    public float floatValue;

    public int value;
    private int lastRealValue;

    private void Start()
    {
        imageRectTransform.gameObject.SetActive(false);

        text.gameObject.SetActive(false);

        SetText();
    }

    public void AddValue(float aDDvalue)
    {
        floatValue += aDDvalue;

        value = Mathf.RoundToInt(floatValue);
        
        if(value > lastRealValue)
        {
            lastRealValue = value;

            SetText();

            imageRectTransform.gameObject.SetActive(true);

            text.gameObject.SetActive(true);
        }


    }

    private void SetText()
    {
        text.text = $"{value}";
    }

    internal void SoldOut()
    {
        //Money.Instance.AddSoft(value);

        //Money.Instance.SaveState();

        var image = imageRectTransform.GetComponent<Image>();

        var rectTransform = GetComponent<RectTransform>();

        var seq = DOTween.Sequence();

        seq.AppendInterval(.15f);

        seq.Append(rectTransform.DOAnchorPos(rectTransform.anchoredPosition + Vector2.up * 150, 1f));
        seq.Join(text.DOFade(0, 1f));
        seq.Join(image.DOFade(0, 1f));
        seq.onComplete += () =>
        {
            gameObject.SetActive(false);

            Destroy(gameObject, 1);
        };

    }
}
