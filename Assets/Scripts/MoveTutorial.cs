using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MoveTutorial : MonoBehaviour
{
    public RectTransform handRectTransform;

    public Canvas canvas;

    private Coroutine packsTutorRoutine, sellTutorRoutine;

    private void Start()
    {
        enabled = false;
    }

    private void Update()
    {
        
    }

    public void SetMoveTutorial(Vector3 worldPos1, Vector3 worldPos2)
    {
        var canvasRect = canvas.GetComponent<RectTransform>().rect;

        var vpPos1 = Camera.main.WorldToViewportPoint(worldPos1);

        var anchorPos1 = new Vector3(vpPos1.x * canvasRect.size.x, vpPos1.y * canvasRect.size.y, 0);

        var vpPos2 = Camera.main.WorldToViewportPoint(worldPos2);

        var anchorPos2 = new Vector3(vpPos2.x * canvasRect.size.x, vpPos2.y * canvasRect.size.y, 0);
    }

    public void StopMoveTutorial()
    {
        if (packsTutorRoutine != null)
        {
            StopCoroutine(packsTutorRoutine);

            packsTutorRoutine = null;
        }

        if(sellTutorRoutine != null)
        {
            StopCoroutine(sellTutorRoutine);

            sellTutorRoutine = null;
        }

        handRectTransform.gameObject.SetActive(false);
    }

    public void SetSellMoveTutorial(Pack pack, Vector3 worldPos)
    {
        if (sellTutorRoutine != null) return;

        enabled = true;

        sellTutorRoutine = StartCoroutine(PackSellMoveRoutine(pack, worldPos));
    }

    private IEnumerator PackSellMoveRoutine(Pack pack1, Vector3 worldPos)
    {
        yield return new WaitForSeconds(.33f);

        var canvasRect = canvas.GetComponent<RectTransform>().rect;

        while (true)
        {
            var vpPos1 = Camera.main.WorldToViewportPoint(pack1.midlePoint);

            var anchorPos1 = new Vector3(vpPos1.x * canvasRect.size.x, vpPos1.y * canvasRect.size.y, 0);

            var vpPos2 = Camera.main.WorldToViewportPoint(worldPos);

            var anchorPos2 = new Vector3(vpPos2.x * canvasRect.size.x, vpPos2.y * canvasRect.size.y, 0);

            handRectTransform.gameObject.SetActive(true);

            handRectTransform.anchoredPosition = anchorPos1;

            handRectTransform.DOAnchorPos(anchorPos2, 1f);

            yield return new WaitForSeconds(1f);

            yield return new WaitForSeconds(.1f);
        }
    }

    public void SetMergeMoveTutorial(Pack pack1, Pack pack2)
    {
        if (packsTutorRoutine != null) return;

        enabled = true;

        packsTutorRoutine = StartCoroutine(PacksMoveRoutine(pack1, pack2));
    }


    private IEnumerator PacksMoveRoutine(Pack pack1, Pack pack2)
    {
        yield return new WaitForSeconds(.33f);

        var canvasRect = canvas.GetComponent<RectTransform>().rect;

        while (true)
        {
            var vpPos1 = Camera.main.WorldToViewportPoint(pack1.midlePoint);

            var anchorPos1 = new Vector3(vpPos1.x * canvasRect.size.x, vpPos1.y * canvasRect.size.y, 0);

            var vpPos2 = Camera.main.WorldToViewportPoint(pack2.midlePoint);

            var anchorPos2 = new Vector3(vpPos2.x * canvasRect.size.x, vpPos2.y * canvasRect.size.y, 0);

            handRectTransform.gameObject.SetActive(true);

            handRectTransform.anchoredPosition = anchorPos1;

            handRectTransform.DOAnchorPos(anchorPos2, 1f);

            yield return new WaitForSeconds(1f);

            yield return new WaitForSeconds(.1f);
        }
    }
}
