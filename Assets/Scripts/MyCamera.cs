using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyCamera : MonoBehaviour
{
    public new Camera camera;

    public float defaultSize = 6.5f;

    private float targetSize, targetYShift;

    public float upgradeShiftSpeed = 20f;

    // Start is called before the first frame update
    void Start()
    {
        targetSize = camera.orthographicSize = GetCameraSize();

        var pos = camera.transform.position;

        targetYShift = pos.y = GetYShift();

        camera.transform.position = pos;
    }

    private void Update()
    {
        camera.orthographicSize = Mathf.MoveTowards(camera.orthographicSize, targetSize, upgradeShiftSpeed * Time.deltaTime);

        var pos = camera.transform.position;

        pos.y = Mathf.MoveTowards(pos.y, targetYShift, upgradeShiftSpeed * .5f * Time.deltaTime);

        camera.transform.position = pos;

        if (camera.orthographicSize == targetSize & pos.y == targetYShift)
        {
            enabled = false;
        }

    }

    public void CupUpgraded()
    {
        StartCoroutine(EnableWithDelay());
    }

    private IEnumerator EnableWithDelay()
    {
        yield return new WaitForSeconds(.3f);

        targetSize = GetCameraSize();

        targetYShift = GetYShift();


        enabled = true;
    }

    public float GetCameraSize()
    {
        //float result = defaultSize;

        //for (int i = 0; i < Control.Instance.cup.currentUpgrade; i++)
        //{
        //    result *= Control.Instance.cup.upgradeScale;
        //}

        return defaultSize * Control.Instance.cup.GetCurrentScale();
    }

    public float GetYShift()
    {
        //float mult = 1f;

        //for (int i = 0; i < Control.Instance.cup.currentUpgrade; i++)
        //{
        //    mult *= Control.Instance.cup.upgradeScale;
        //}

        var centerY = Control.Instance.cup.centerTransform.position.y;

        var offsetY = Control.Instance.cup.cameraOffset.y;

        //Debug.Log(centerY);
        ///Debug.Log(offsetY);

        return centerY - offsetY * Control.Instance.cup.GetCurrentScale();///*mult*/;
    }
}
