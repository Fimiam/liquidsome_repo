using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;


public class SuggestValue : MonoBehaviour
{
    public static int saturProp;

    public TextMeshPro softText, hardText;

    public SpriteRenderer hardSprite;

    public Color rewardSoftColor = Color.green;

    private void Awake()
    {
        saturProp = Shader.PropertyToID("_Saturation");
    }

    //private int 

    public void SetupSuggest(int softValue, int hardValue)
    {
        gameObject.SetActive(true);

        softText.text = $"<size={softText.fontSize - .3f}>$</size>{Control.Instance.GetShrinkNumberString(softValue)}";

        if(hardValue > 0)
        {
            hardText.text = $"{Control.Instance.GetShrinkNumberString(hardValue)}";

            hardSprite.gameObject.SetActive(true);

            //hardSprite.material = hardSpriteMaterial = new Material(hardSprite.material);
        }
        else
        {
            hardSprite.gameObject.SetActive(false);
        }
    }

    public void ShowReward()
    {
        var rew = Instantiate(gameObject).GetComponent<SuggestValue>();

        rew.transform.parent = transform.parent;

        rew.transform.position = transform.position;

        rew.softText.color = rewardSoftColor;

        if(hardSprite.gameObject.activeSelf)
        {
            rew.hardSprite.material = new Material(hardSprite.material);

            rew.hardSprite.material.SetFloat(saturProp, 1f);
        }

        var seq = DOTween.Sequence(rew);

        seq.Append(rew.transform.DOScale(transform.localScale.x * 1.66f, .033f));
        seq.AppendInterval(.5f);
        seq.Append(rew.softText.DOFade(0, .15f));

        if(hardSprite.gameObject.activeSelf)
        {
            seq.Join(rew.hardText.DOFade(0, .15f));
            seq.Join(rew.hardSprite.DOFade(0, .15f));
        }

        seq.Join(rew.transform.DOMoveY(rew.transform.position.y + .25f, .15f));

        seq.onComplete += () => Destroy(rew.gameObject);

        gameObject.SetActive(false);
    }
}
