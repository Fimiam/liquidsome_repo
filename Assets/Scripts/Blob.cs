using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blob : MonoBehaviour
{
    public SpriteRenderer colorSprite;

    public Pack pack;

    public Rigidbody2D body;

    public float defaultSpringValue, holdSpringValue;

    public float defaltGravity = 1, freeFallGravity = 2;

    //public float defaultDrag = 0f, holdDrag = 2f;

    public float holdDragDistance = 5;

    //private Vector2 holdForce = Vector2.zero;

    public int number;

    public SpriteRenderer sellingSignSprite;

    public void Setup(Pack pack, int number, Color color)
    {
        this.pack = pack;
        this.number = number;

        //colorSprite.color = color;

        StartCoroutine(ColorLerp(color, .1f));

        transform.SetParent(pack.transform);

        sellingSignSprite.gameObject.SetActive(pack.mergableData.softValue > 0);
    }

    private IEnumerator ColorLerp(Color toColor, float duration)
    {
        float t = 0f;

        Color startColor = colorSprite.color;

        while(t < duration)
        {
            t += Time.deltaTime;

            colorSprite.color = Color.Lerp(startColor, toColor, t / duration);

            yield return null;
        }

        colorSprite.color = pack.color;
    }

    internal void Sell(int price, SellPoint sellPoint)
    {
        StartCoroutine(Selling(price, .05f, () =>
        {
            sellPoint.Sold(this);

            pack.BlobSold(this);

            gameObject.SetActive(false);

            Destroy(gameObject, 1);

        }));
    }

    private IEnumerator Selling(int price, float duration = .2f, Action doneCallback = null)
    {
        float t = 0f;

        while(t < duration)
        {
            yield return null;

            t += Time.deltaTime;

            transform.localScale = Vector2.one * (1f - t / duration);
        }

        doneCallback?.Invoke();
    }

    public void FixedUpdate()
    {
        var toPoint = pack.midlePoint - body.position;

        var force = toPoint.normalized * (defaultSpringValue + (pack.hold ? holdSpringValue : 0f));

       // force += holdForce;

        body.AddForce(force, ForceMode2D.Impulse);

        var gravity = defaltGravity;

        //var drag = defaultDrag;

        var distance = toPoint.magnitude;

        var il = Mathf.InverseLerp(holdDragDistance, 0, distance);

        //colorSprite.color = Color.Lerp(Color.black, Color.red, il);

        if (pack.hold)
        {

            //drag = Mathf.Lerp(defaultDrag, holdDrag, il);

            //var delta = flow.flowBounds.max.y - body.position.y;

            // var il = Mathf.InverseLerp(0f, 5f, delta);

            //gravity = 0;// Mathf.Lerp(defaltGravity, freeFallGravity, il);
        }

        //body.drag = drag;

        body.gravityScale = gravity;


    }

    //internal void ToHoldPoint(Vector2 point)
    //{
    //    var toPoint = point - body.position;

    //    var force = toPoint.normalized * holdSpringValue;

    //    body.AddForce(force, ForceMode2D.Force);
    //}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(!pack.inSell & !pack.inMerge & collision.collider.TryGetComponent<Blob>(out var blob))
        {
            if(blob.pack != pack & !blob.pack.inMerge & !blob.pack.inSell)
            {
                if(blob.pack.id == pack.id)
                {
                    Control.Instance.MergePacks(blob.pack, pack, body.position);
                }
            }
        }
    }
}


