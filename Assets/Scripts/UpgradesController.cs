using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradesController : MonoBehaviour
{
    public int autoclickUpgrade;
    public List<AutoclickUpgradeData> autoclickDataList;
    public bool autoclickActive = true;

    public int doublespawnUpgrade;
    public List<DoublespawnUpgradeData> doublespawnDataList;

    public int profitUpgrade;
    public List<ProfitUpgradeData> profitDataList;

    public int expandDiscUpgrade;
    public List<ExpandDiscUpgradeData> expandDiscDataList;

    //public Action onItemsAddUpgrade;


    private void Awake()
    {
        autoclickUpgrade = PlayerPrefs.GetInt("autoclick");
        autoclickActive = PlayerPrefs.GetInt("aut_click_active", 1) > 0;

        doublespawnUpgrade = PlayerPrefs.GetInt("doublespawn");

        profitUpgrade = PlayerPrefs.GetInt("profit");

        expandDiscUpgrade = PlayerPrefs.GetInt("exp_disc");
    }

    public void SetAutoclickActive(bool active)
    {
        autoclickActive = active;

        PlayerPrefs.SetInt("aut_click_active", active ? 1 : 0);
    }

    public ExpandDiscUpgradeData GetExpandDiscData()
    {
        ExpandDiscUpgradeData data = null;

        if (expandDiscUpgrade > expandDiscDataList.Count - 1)
            data = expandDiscDataList[expandDiscDataList.Count - 1];
        else
        {
            data = expandDiscDataList[expandDiscUpgrade];
        }

        return data;
    }

    public ProfitUpgradeData GetProfitData()
    {
        ProfitUpgradeData data = null;

        if (profitUpgrade > profitDataList.Count - 1)
            data = profitDataList[profitDataList.Count - 1];
        else
        {
            data = profitDataList[profitUpgrade];
        }

        return data;
    }

    public DoublespawnUpgradeData GetDoublespawnData()
    {
        DoublespawnUpgradeData data = null;

        if (doublespawnUpgrade > doublespawnDataList.Count - 1)
            data = doublespawnDataList[doublespawnDataList.Count - 1];
        else
        {
            data = doublespawnDataList[doublespawnUpgrade];
        }

        return data;
    }

    public AutoclickUpgradeData GetAutoclickData()
    {
        AutoclickUpgradeData data = null;

        if (autoclickUpgrade > autoclickDataList.Count - 1)
            data = autoclickDataList[autoclickDataList.Count - 1];
        else
        {
            data = autoclickDataList[autoclickUpgrade];
        }

        return data;
    }

    public float GetExpandDiscPercent()
    {
        return expandDiscUpgrade > 0 ? expandDiscDataList[expandDiscUpgrade - 1].percent: 1f;
    }

    public float GetProfitChance()
    {
        return profitUpgrade > 0 ? profitDataList[profitUpgrade - 1].chance + .025f : -1f;
    }

    public float GetDoublespawnChance()
    {
        return doublespawnUpgrade > 0 ? doublespawnDataList[doublespawnUpgrade - 1].chance + .025f : -1f;
    }

    public float GetAutoclickRate()
    {
        return autoclickUpgrade > 0 ? autoclickDataList[autoclickUpgrade - 1].secondsRate : -1f;
    }

    public void UpgradeAutoclick()
    {
        var data = GetAutoclickData();

        if(data.priceSoft > 0)
        {
            if (Money.Instance.softValue < data.priceSoft) return;

            autoclickUpgrade++;

            Money.Instance.UseSoft(data.priceSoft);

            PlayerPrefs.SetInt("autoclick", autoclickUpgrade);
        }
        else
        {
            if (Money.Instance.hardValue < data.priceHard) return;

            autoclickUpgrade++;

            Money.Instance.UseHard(data.priceHard);

            PlayerPrefs.SetInt("autoclick", autoclickUpgrade);
        }

        //onItemsAddUpgrade?.Invoke();

        Control.Instance.MadeUpgrade();
    }

    internal void UpgradeDoublespawn()
    {
        var data = GetDoublespawnData();

        if (data.priceSoft > 0)
        {
            if (Money.Instance.softValue < data.priceSoft) return;

            doublespawnUpgrade++;

            Money.Instance.UseSoft(data.priceSoft);

            PlayerPrefs.SetInt("doublespawn", doublespawnUpgrade);
        }
        else
        {
            if (Money.Instance.hardValue < data.priceHard) return;

            doublespawnUpgrade++;

            Money.Instance.UseHard(data.priceHard);

            PlayerPrefs.SetInt("doublespawn", doublespawnUpgrade);
        }

        Control.Instance.MadeUpgrade();
    }

    internal void UpgradeProfit()
    {
        var data = GetProfitData();

        if (data.priceSoft > 0)
        {
            if (Money.Instance.softValue < data.priceSoft) return;

            profitUpgrade++;

            Money.Instance.UseSoft(data.priceSoft);

            PlayerPrefs.SetInt("profit", profitUpgrade);
        }
        else
        {
            if (Money.Instance.hardValue < data.priceHard) return;

            profitUpgrade++;

            Money.Instance.UseHard(data.priceHard);

            PlayerPrefs.SetInt("profit", profitUpgrade);
        }

        Control.Instance.MadeUpgrade();
    }

    internal void UpgradeExpandDisc()
    {
        var data = GetExpandDiscData();

        if (data.priceSoft > 0)
        {
            if (Money.Instance.softValue < data.priceSoft) return;

            expandDiscUpgrade++;

            Money.Instance.UseSoft(data.priceSoft);

            PlayerPrefs.SetInt("exp_disc", expandDiscUpgrade);
        }
        else
        {
            if (Money.Instance.hardValue < data.priceHard) return;

            expandDiscUpgrade++;

            Money.Instance.UseHard(data.priceHard);

            PlayerPrefs.SetInt("exp_disc", expandDiscUpgrade);
        }

        Control.Instance.MadeUpgrade();
    }
}

[System.Serializable]
public class AutoclickUpgradeData
{
    public int priceSoft, priceHard;

    public float secondsRate;
}

[System.Serializable]
public class DoublespawnUpgradeData
{
    public int priceSoft, priceHard;

    public float chance;
}

[System.Serializable]
public class ProfitUpgradeData
{
    public int priceSoft, priceHard;

    public float chance;
}

[System.Serializable]
public class ExpandDiscUpgradeData
{
    public int priceSoft, priceHard;

    public float percent;
}
