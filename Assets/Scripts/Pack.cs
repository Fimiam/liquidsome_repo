using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pack : MonoBehaviour
{
    public List<Blob> blobs;

    public Vector2 midlePoint;

    public bool hold;

    //public Bounds flowBounds;

    public Vector2 holdPoint;

    public int id;
    public bool inMerge;
    public Color color;
    public bool inSell;
    public int blobPrice = 1;

    public float defaultDrag = 0, holdDrag = 5;

    public MergableItemData mergableData;

    private Cup cup;

    //public int sortingOrder;

    private void Start()
    {

    }

    public void SetupData(MergableItemData data)
    {
        this.id = data.id;
        this.color = Control.Instance.GetColorById(id);
        mergableData = data;
    }

    public void Setup(Cup cup, MergableItemData data, int count)
    {
        this.id = data.id;
        this.color = Control.Instance.GetColorById(id);
        this.cup = cup;
        mergableData = data;

        transform.SetParent(cup.packsContainer);

        //sortingOrder = cup.currentSortingOrder;

        while (count > blobs.Count)
        {
            var newBlob = Instantiate(blobs[0], blobs[0].body.position, Quaternion.identity, transform);

            blobs.Add(newBlob);
        }

        while (count < blobs.Count)
        {
            var toRemove = blobs[0];

            blobs.RemoveAt(0);

            toRemove.gameObject.SetActive(false);

            Destroy(toRemove.gameObject, 1);
        }

        foreach (var blob in blobs)
        {
            blob.Setup(this, id, color);
        }


        SpawnShake();
        
        cup.AddPack(this);

        //cup.AddValue(blobs.Count);

        SetHold(false);
    }

    internal int GetHardValue()
    {
        return mergableData.hardValue;
    }

    internal void BlobSold(Blob blob)
    {
        //blobs.Remove(blob);
        //cup.RemoveValue(1);
    }

    public void SetHold(bool hold)
    {
        this.hold = hold;

        if(hold)
        {
            foreach (var item in blobs)
            {
                item.body.drag = holdDrag;
            }
        }
        else
        {
            foreach (var item in blobs)
            {
                item.body.drag = defaultDrag;
            }
        }
    }

    internal void BeginSell()
    {
        cup.RemovePack(this);
        cup.RemovePersist(id);

        inSell = true;

        foreach (var item in blobs)
        {
            //item.body.velocity = Vector2.zero;

            item.body.drag = holdDrag;

            item.defaultSpringValue *= 3f;

            item.body.GetComponent<Collider2D>().enabled = false;
        }

        Control.Instance.SellingPack(this);
    }

    internal int GetSoftValue()
    {
        return mergableData.softValue;
    }

    public void SpawnShake()
    {
        for (int i = 0; i < blobs.Count; i++)
        {
            if(i % 2 == 0)
            {
                blobs[i].body.AddForce(Random.insideUnitCircle * 10, ForceMode2D.Impulse);
            }
        }
    }

    public void RemoveBlobAt(int index)
    {
        blobs.RemoveAt(index);

        Control.Instance.cup.RemoveValue(1);
    }

    public void AddBlob(Blob blob)
    {
        blobs.Add(blob);

        blob.Setup(this, id, color);

        cup.AddValue(1);
    }

    private void FixedUpdate()
    {
        if (inSell) return;

        //flowBounds = new Bounds(blobs[0].body.position, Vector3.zero);

        if(blobs.Count > 0)
        {
            midlePoint = Vector2.zero;
            foreach (var blob in blobs)
            {
                //flowBounds.Encapsulate(blob.body.position);
                midlePoint += (Vector2)blob.transform.position;
            }

            //midlePoint = flowBounds.center;
            midlePoint /= blobs.Count;
        }


        //Debug.DrawRay(midlePoint, Vector3.up, Color.red);

        if(hold)
        {
            holdPoint = Control.Instance.GetInputPoint();

            midlePoint = Vector2.Lerp(midlePoint, holdPoint, 1);

            if (midlePoint.y > Control.Instance.cup.topPoint.position.y)
            {
                midlePoint.y = Control.Instance.cup.topPoint.position.y;
            }

            //foreach(var blob in blobs)
            //{
            //    blob.ToHoldPoint(point);
            //}
        }
    }

    internal void Sold()
    {
        Control.Instance.cup.RemovePack(this);

        gameObject.SetActive(false);

        Destroy(gameObject, 1);
    }
}
