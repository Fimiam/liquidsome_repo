using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemValueData", menuName = "ItemValueData")]
public class MergableItemData : ScriptableObject
{
    public int id;
    public MergableItemData mergeToData;
    public int softValue;
    public int hardValue;
    public int mergedCount;
    public Color color;
}
