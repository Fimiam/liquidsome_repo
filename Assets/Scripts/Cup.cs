using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using System.Linq;

public class Cup : MonoBehaviour
{
    public Transform spawnPointLeft, spawnPointRight, topPoint, tutorFirstPackPoint, tutorSecondPackPoint, centerTransform;

    //public float maxOffset;

    public Pack packPrefab;

    public TextMeshPro fillText;

    public int current, max;

    public Color defaultTextColor = Color.white;
    public Color filledTextColor = Color.red;

    public bool filled => current >= max;

    public List<Pack> packs = new List<Pack>();

    public List<CupUpgrades> upgrades;

    public int currentUpgrade;

    public Transform packsContainer;

    public Vector3 cameraOffset;

    public float upgradeScale = 1.1f;

    private UpgradesController upgradesController;

    public CupPersist persist;

    private void Awake()
    {
        currentUpgrade = PlayerPrefs.GetInt("cup_upgrade");

        max = upgrades[currentUpgrade].maxItems;

        cameraOffset = centerTransform.position;

        transform.localScale = GetCurrentScale() * Vector3.one;

        upgradesController = FindObjectOfType<UpgradesController>();

        if(PlayerPrefs.HasKey("cup_persist"))
        {
            var str = PlayerPrefs.GetString("cup_persist");

            persist = JsonUtility.FromJson<CupPersist>(str);
        }
        else
        {
            persist = new CupPersist();

            persist.packs = new List<int>();
        }
    }

    private void Start()
    {
        SetText();

        StartCoroutine(SavePersistRoutine());

        if(persist.packs.Count > 0)
        {
            StartCoroutine(SpawnPersist());
        }

        if(!Control.Instance.mergeTutorialDone)
        {
            var itemData = Control.Instance.colorList[0];

            var worldPos = tutorFirstPackPoint.position;

            var pack = Instantiate(packPrefab);

            pack.transform.position = worldPos;

            pack.Setup(this, itemData, Control.Instance.itemsCount);

            StartCoroutine(FirstTutorPackAlignment(pack));

        }
    }

    private IEnumerator SpawnPersist()
    {
        foreach(var item in persist.packs)
        {
            var data = Control.Instance.colorList.Find(i => i.id == item);

            SpawnPack(data, true);

            yield return new WaitForSeconds(.1f);
        }
    }

    private IEnumerator SavePersistRoutine()
    {
        while(true)
        {
            yield return new WaitForSeconds(10);

            SavePersist();
        }
    }

    public void AddPersist(int id)
    {
        persist.packs.Add(id);
    }

    public void RemovePersist(int id)
    {
        persist.packs.Remove(id);
    }

    public void SavePersist()
    {
        var str = JsonUtility.ToJson(persist);

        PlayerPrefs.SetString("cup_persist", str);
    }

    public int GetUpgradePrice()
    {
        var price = upgrades[currentUpgrade].price;

        if(upgradesController.expandDiscUpgrade > 0)
        {
            price = Mathf.RoundToInt(price * upgradesController.GetExpandDiscPercent());
        }

        return price;
    }

    public void SetText()
    {
        fillText.text = $"{current}/{max}";

        fillText.color = filled ? filledTextColor : defaultTextColor;
    }

    private IEnumerator FirstTutorPackAlignment(Pack pack)
    {
        while (Control.Instance.controlPack == null)
        {
            foreach (var item in pack.blobs)
            {
                if (item.body.position.x < 1.5f)
                {
                    item.body.AddForce(Vector2.right * 25);
                }
            }

            yield return null;
        }
    }

    public void NoSpaceShake()
    {
        fillText.transform.DOComplete();

        fillText.transform.DOShakePosition(1f, .5f);

        VibrationManager.instance.VibroAddFail();
    }

    public void Upgrade()
    {
        if (!Control.Instance.expandTutorialDone) Control.Instance.ExpandTutorialDone();

        currentUpgrade++;

        PlayerPrefs.SetInt("cup_upgrade", currentUpgrade);

        max = upgrades[currentUpgrade].maxItems;

        SetText();

        Control.Instance.CupUpgraded();

        transform.DOComplete();

        transform.DOScale(transform.localScale.x * upgradeScale, .1f);
    }

    public float GetCurrentScale()
    {
        float result = 1;

        for (int i = 0; i < currentUpgrade; i++)
        {
            result *= upgradeScale;
        }

        return result;
    }

    public void SpawnSecondTutorPack()
    {
        var itemData = Control.Instance.colorList[0];

        var worldPos = tutorSecondPackPoint.position;

        var pack = Instantiate(packPrefab);

        pack.transform.position = worldPos;

        pack.Setup(this, itemData, Control.Instance.itemsCount);

        //currentSortingOrder++;

        StartCoroutine(SecondTutorPackAlignment(pack));
    }

    private IEnumerator SecondTutorPackAlignment(Pack pack)
    {
        while(Control.Instance.controlPack == null)
        {

            foreach (var item in pack.blobs)
            {
                if(item.body.position.x > -1.5f)
                {
                    item.body.AddForce(Vector2.left * 25);
                }
            }

            yield return null;
        }
    }

    public bool HavePacksToMerge()
    {
        foreach (var p1 in packs)
        {
            foreach (var p2 in packs)
            {
                if (p1 != p2 && p1.id == p2.id) return true;
            }
        }

        return false;
    }

    public bool HavePacksToSell()
    {
        foreach (var item in packs)
        {
            if(item.mergableData.softValue > 0)
            {
                return true;
            }
        }

        return false;
    }

    public List<Pack> GetPacksForSell()
    {
        var list = new List<Pack>();

        foreach (var item in packs)
        {
            if(item.mergableData.softValue > 0)
            {
                list.Add(item);
            }
        }

        return list;
    }

    internal void AddPack(Pack pack)
    {
        packs.Add(pack);

        AddValue(pack.blobs.Count);
    }

    internal void RemovePack(Pack pack)
    {
        packs.Remove(pack);

        RemoveValue(pack.blobs.Count);
    }

    public void AddValue(int val)
    {
        current += val;

        SetText();
    }

    public void RemoveValue(int val)
    {
        current -= val;

        //if (current < 0) current = 0;

        SetText();
    }

    public void SpawnPack(MergableItemData itemData, bool fromPersist = false)
    {
        var worldPos = Vector3.Lerp(spawnPointLeft.position, spawnPointRight.position, Random.value);

        var pack = Instantiate(packPrefab);

        pack.transform.position = worldPos;

        if(fromPersist)
        {
            pack.Setup(this, itemData, itemData.mergedCount);
        }
        else
        {
            pack.Setup(this, itemData, Control.Instance.itemsCount);

            AddPersist(itemData.id);
        }


        //currentSortingOrder++;
    }

    private void OnApplicationQuit()
    {
        SavePersist();
    }

    private void OnApplicationPause(bool pause)
    {
        SavePersist();
    }
}

[System.Serializable]
public class CupUpgrades
{
    public int price;
    public int maxItems;
}

[System.Serializable]
public class CupPersist
{
    public List<int> packs;
}
