using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class SellSuggest : MonoBehaviour
{
    public List<Blob> blobsInRange = new List<Blob>();

    private Vector3 suggestPosition;

    public SuggestValue suggestValue;

    private void Update()
    {
        if (blobsInRange.Count < 1)
        {
            suggestValue.transform.localScale = Vector3.MoveTowards(suggestValue.transform.localScale, Vector3.zero, 15 * Time.deltaTime);

            suggestValue.transform.localPosition = Vector3.MoveTowards(suggestValue.transform.localPosition, Vector3.zero, 15 * Time.deltaTime);

            if (suggestValue.transform.localScale.z == 0)
            {
                enabled = false;
            }
        }
        else
        {
            suggestValue.transform.localScale = Vector3.MoveTowards(suggestValue.transform.localScale, Vector3.one, 15 * Time.deltaTime);

            suggestValue.transform.localPosition = Vector3.MoveTowards(suggestValue.transform.localPosition, suggestPosition, 15 * Time.deltaTime);

            //suggestPriceText.text = $"<size={suggestPriceText.fontSize - 1}>$</size>{suggestSoftValue}";
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.TryGetComponent<Blob>(out var blob))
        {
            //Debug.Log(blob.name);

            if (blob.pack == Control.Instance.controlPack & blob.pack.mergableData.softValue > 0)
            {

                if (blobsInRange.Count == 0)
                {
                    blobsInRange.Add(blob);

                    suggestValue.SetupSuggest(blob.pack.GetSoftValue(), blob.pack.GetHardValue());

                    suggestPosition = Vector2.left * 2.5f;

                    enabled = true;
                }
                else
                {
                    if (blobsInRange[0].pack == blob.pack) blobsInRange.Add(blob);
                }
            }
        }
    }

    internal void SellReady()
    {
        suggestValue.transform.localScale = Vector3.one;

        suggestValue.transform.DOComplete();

        suggestValue.transform.DOPunchScale(Vector3.one * .6f, .2f, 1);

        VibrationManager.instance.SellReadyVibro();
    }

    internal void Sold()
    {
        suggestValue.ShowReward();
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.TryGetComponent<Blob>(out var blob))
        {
            blobsInRange.Remove(blob);
        }
    }
}
