using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Liquid : MonoBehaviour
{
    public Camera cameraHeight, cameraColor;

    private float defaultSize;

    private void Start()
    {
        defaultSize = cameraHeight.orthographicSize;

        cameraColor.orthographicSize = cameraHeight.orthographicSize = defaultSize * Control.Instance.cup.GetCurrentScale();
    }

    public void CupUpgraded()
    {
        var size = defaultSize * Control.Instance.cup.GetCurrentScale();

        cameraHeight.DOOrthoSize(size, .1f);
        cameraColor.DOOrthoSize(size, .1f);
    }
}
