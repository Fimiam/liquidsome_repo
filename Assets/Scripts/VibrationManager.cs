using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;
using System;

public class VibrationManager : MonoBehaviour
{
    public static VibrationManager instance;

    [SerializeField] private float androidVibroDelay = .2f;
    [SerializeField] private float mergeVibroDelay = .2f;

    private float lastVibro, lastMergeVibro;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(gameObject);
        }
        else if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void VibroMerge()
    {
        if (!Control.Instance.vibroActive) return;

        //#if UNITY_ANDROID
        //        if (Time.time < lastVibro + androidVibroDelay) return;
        //#endif

        if (Time.time < lastMergeVibro + mergeVibroDelay) return;

        lastMergeVibro = Time.time;

        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
    }

    public void VibroAddFail()
    {
        if (!Control.Instance.vibroActive) return;

        MMVibrationManager.Haptic(HapticTypes.Warning);
    }

    public void VibroLevelEnd()
    {
        if (!Control.Instance.vibroActive) return;

        MMVibrationManager.Haptic(HapticTypes.Success);
    }

    internal void VibroNoMoney()
    {
        if (!Control.Instance.vibroActive) return;

        MMVibrationManager.Haptic(HapticTypes.Warning);
    }

    public void VibroClick()
    {
        if (!Control.Instance.vibroActive) return;

        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
        //MMVibrationManager.Haptic(HapticTypes.Selection);
    }

    internal void SellReadyVibro()
    {
        if (!Control.Instance.vibroActive) return;

        MMVibrationManager.Haptic(HapticTypes.MediumImpact);
    }

    internal void SellingVibro()
    {
        if (!Control.Instance.vibroActive) return;

        MMVibrationManager.Haptic(HapticTypes.Success);
    }
}
