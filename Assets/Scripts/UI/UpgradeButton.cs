using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButton : MonoBehaviour
{
    public UpgradePanel upgradePanel;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            if (Control.Instance.buttonTutorialActive ||
            !Control.Instance.addTutorialDone ||
            !Control.Instance.mergeTutorialDone ||
            !Control.Instance.sellTutorialDone) return;

            upgradePanel.Show();

            VibrationManager.instance.VibroClick();
        });
    }
}
