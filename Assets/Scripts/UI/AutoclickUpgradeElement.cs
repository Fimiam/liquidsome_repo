using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoclickUpgradeElement : UpgradeElement
{
    public NeatToggle autoclickToggle;

    public AddPackButton addPackButton;

    public override void Start()
    {
        base.Start();

        addPackButton = FindObjectOfType<AddPackButton>();

        autoclickToggle.SetValue(upgradesController.autoclickActive, true);

        autoclickToggle.OnValueChange += (v) =>
        {
            upgradesController.SetAutoclickActive(v);

            Refresh();

            addPackButton.Refresh();
        };
    }

    public override void Refresh()
    {
        var upgradeLevel = Control.Instance.upgradesController.autoclickUpgrade;

        var data = upgradesController.GetAutoclickData();

        if(upgradeLevel == 0)
        {
            description_text.text = "Add slimes with auto-click!";
        }
        else
        {
            var lastUpdate = upgradeLevel >= upgradesController.autoclickDataList.Count - 1;

            if(lastUpdate)
            {
                upgradeButton.gameObject.SetActive(false);

                description_text.gameObject.SetActive(false);
            }
            else
            {
                description_text.text = $"Reduce auto-click rate" +
                    $" [{upgradesController.autoclickDataList[upgradeLevel - 1].secondsRate}s -> {data.secondsRate}s]";
            }
        }

        if(data.priceSoft > 0)
        {
            hardObject.gameObject.SetActive(false);
            softObject.gameObject.SetActive(true);

            soft_text.text = $"{Control.Instance.GetShrinkNumberString(data.priceSoft)}";

            available = data.priceSoft <= Money.Instance.softValue;

            soft_text.color = available ? availableColor : unavailableColor;
        }
        else
        {
            hardObject.gameObject.SetActive(true);
            softObject.gameObject.SetActive(false);

            hard_text.text = $"{Control.Instance.GetShrinkNumberString(data.priceHard)}";

            available = data.priceHard <= Money.Instance.hardValue;

            hard_text.color = available ? availableColor : unavailableColor;
        }

        autoclickToggle.gameObject.SetActive(upgradeLevel > 0);
        
        if(autoclickToggle.active != upgradesController.autoclickActive)
        {
            autoclickToggle.SetValue(upgradesController.autoclickActive);
        }
    }

    public override void UpgradeClick()
    {
        if(available)
        {
            upgradesController.UpgradeAutoclick();

            base.UpgradeClick();
        }
        else
        {
            Money.Instance.NoMoneyShake();
        }
    }
}
