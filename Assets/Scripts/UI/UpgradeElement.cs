using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UpgradeElement : MonoBehaviour
{
    public GameObject softObject, hardObject;

    public TextMeshProUGUI soft_text, hard_text, description_text;

    public Color availableColor = Color.white, unavailableColor = Color.red;

    public Button upgradeButton;

    protected UpgradesController upgradesController;

    public bool available;

    public virtual void Awake()
    {
        upgradeButton.onClick.AddListener(UpgradeClick);

        Money.Instance.onMoneyChanged += Refresh;
    }

    public virtual void Start()
    {
        upgradesController = Control.Instance.upgradesController;

        Refresh();
    }

    public virtual void Refresh()
    {

    }

    public virtual void UpgradeClick()
    {
        VibrationManager.instance.VibroClick();
    }
}
