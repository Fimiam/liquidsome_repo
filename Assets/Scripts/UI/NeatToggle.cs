using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class NeatToggle : MonoBehaviour
{
    [SerializeField] private Button button;

    [SerializeField] private Image backgroundImage;

    [SerializeField] private RectTransform switchRect;

    [SerializeField] private Vector2 activePosition, disactivePosition;

    [SerializeField] private float targetForce = 12, targetDamp = .1f;

    public Color activeColor, disactiveColor;

    public bool active;

    public Action<bool> OnValueChange;

    private Vector2 switchTarget, switchVelocity;

    private void Start()
    {
        button.onClick.AddListener(() => 
        {
            SetValue(!active);

            OnValueChange?.Invoke(active);
        });
    }

    private void Update()
    {
        var pos = switchRect.anchoredPosition;

        Springing.CalcDampedSimpleHarmonicMotion
            (ref pos.x, ref switchVelocity.x, switchTarget.x, Time.deltaTime, targetForce, targetDamp);

        switchRect.anchoredPosition = pos;
    }

    public void SetValue(bool value, bool fast = false)
    {
        active = value;

        backgroundImage.color = value ? activeColor : disactiveColor;

        if(fast)
        {
            switchTarget = switchRect.anchoredPosition = value ? activePosition : disactivePosition;
        }
        else
        {
            switchTarget = value ? activePosition : disactivePosition;
        }
    }
}
