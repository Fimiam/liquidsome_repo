using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpandUpgradeElement : UpgradeElement
{

    private ExpandButton expandButton;

    public override void Start()
    {
        base.Start();

        expandButton = FindObjectOfType<ExpandButton>();
    }

    public override void Refresh()
    {
        var upgradeLevel = Control.Instance.upgradesController.expandDiscUpgrade;

        var data = upgradesController.GetExpandDiscData();

        if (upgradeLevel == 0)
        {
            description_text.text = "Expand cost has a discount!";
        }
        else
        {
            var lastUpdate = upgradeLevel >= upgradesController.profitDataList.Count - 1;

            if (lastUpdate)
            {
                upgradeButton.gameObject.SetActive(false);

                description_text.gameObject.SetActive(false);
            }
            else
            {
                description_text.text = $"Expand cost discount" +
                    $" [{upgradesController.expandDiscDataList[upgradeLevel - 1].percent * 100f}% -> {data.percent * 100f}%]";
            }
        }

        if (data.priceSoft > 0)
        {
            hardObject.gameObject.SetActive(false);
            softObject.gameObject.SetActive(true);

            soft_text.text = $"{Control.Instance.GetShrinkNumberString(data.priceSoft)}";

            available = data.priceSoft <= Money.Instance.softValue;

            soft_text.color = available ? availableColor : unavailableColor;
        }
        else
        {
            hardObject.gameObject.SetActive(true);
            softObject.gameObject.SetActive(false);

            hard_text.text = $"{Control.Instance.GetShrinkNumberString(data.priceHard)}";

            available = data.priceHard <= Money.Instance.hardValue;

            hard_text.color = available ? availableColor : unavailableColor;
        }
    }

    public override void UpgradeClick()
    {
        if (available)
        {
            upgradesController.UpgradeExpandDisc();

            base.UpgradeClick();

            expandButton.Refresh();
        }
        else
        {
            Money.Instance.NoMoneyShake();
        }
    }
}
