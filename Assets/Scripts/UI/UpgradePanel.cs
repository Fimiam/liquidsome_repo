using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UpgradePanel : MonoBehaviour
{
    [SerializeField] private CanvasGroup viewGroup;

    [SerializeField] private RectTransform containerRect;

    [SerializeField] private Button closeButton;

    [SerializeField] private float fadeInDuration, fadeOutDuration;

    [SerializeField] private float posForce = 15, posDump = .4f;

    private Vector2 targetPos, currentVelocity;

    // Start is called before the first frame update
    void Start()
    {
        closeButton.onClick.AddListener(() => 
        {
            VibrationManager.instance.VibroClick();

            Hide(); 
        });
    }

    private void Update()
    {
        var pos = containerRect.localPosition;

        Springing.CalcDampedSimpleHarmonicMotion
            (ref pos.x, ref currentVelocity.x, targetPos.x, Time.deltaTime, posForce, posDump);

        containerRect.localPosition = pos;

        if (Input.GetMouseButtonDown(0))
        {
            PointerEventData pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1,
            };

            pointerData.position = Input.mousePosition;

            List<RaycastResult> results = new List<RaycastResult>();

            EventSystem.current.RaycastAll(pointerData, results);

            if (!results.Exists(r => r.gameObject == containerRect.gameObject))
            {
                Hide();
            }

        }
    }

    public void Show()
    {
        viewGroup.gameObject.SetActive(true);

        enabled = true;

        viewGroup.interactable = true;

        viewGroup.DOFade(1, fadeInDuration);

        targetPos = Vector2.zero;

        containerRect.localPosition = Vector2.right * 1000;

        VibrationManager.instance.VibroClick();
    }

    public void Hide()
    {
        targetPos = Vector2.right * -1000;

        viewGroup.interactable = false;

        viewGroup.DOFade(0, fadeOutDuration).onComplete += () =>
        {
            viewGroup.gameObject.SetActive(false);
            enabled = false;
        };
    }
}
