using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ExpandButton : MonoBehaviour
{
    public TextMeshProUGUI price_text;

    public Image bg_image, money_image;

    private int saturationProp;

    public bool haveMoney;

    public GameObject tutorHand;

    private void Awake()
    {
        saturationProp = Shader.PropertyToID("_Saturation");
    }

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            if(!haveMoney)
            {
                Money.Instance.NoMoneyShake();

                return;
            }

            Money.Instance.UseSoft(Control.Instance.cup.GetUpgradePrice());

            Control.Instance.cup.Upgrade();



            Refresh();


            VibrationManager.instance.VibroClick();
        });


        Refresh();

        Money.Instance.onMoneyChanged += Refresh;
    }

    public void Refresh()
    {
        var soft = Money.Instance.softValue;

        if(!Control.Instance.expandTutorialDone)
        {
            if(Control.Instance.cup.GetUpgradePrice() + (Control.Instance.itemsCost * 2) <= soft)
            {
                Control.Instance.buttonTutorialActive = true;

                tutorHand.SetActive(true);
            }
            else
            {
                tutorHand.SetActive(false);

                Control.Instance.buttonTutorialActive = false;
            }
        }
        else
        {
            tutorHand.SetActive(false);
        }

        var cost = Control.Instance.cup.GetUpgradePrice();

        price_text.text = $"{Control.Instance.GetShrinkNumberString(cost)}";

        haveMoney = soft >= cost;

        var material = new Material(bg_image.material);

        material.SetFloat(saturationProp, haveMoney ? 1 : 0);

        bg_image.material = material;
        money_image.material = material;
    }
}
