using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class AddPackButton : MonoBehaviour
{
    public TextMeshProUGUI blobsCount_text, price_text;

    public Image bg_image, items_image, money_image;

    private int saturationProp;

    public bool haveMoney;

    public GameObject tutorHand;

    private float autoclickRate;

    private Coroutine autoclickRoutine;

    //public RectTransform autoclickRectTransform;

    private void Awake()
    {
        saturationProp = Shader.PropertyToID("_Saturation");    
    }

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(() =>
        {
            if (tutorHand.activeSelf)
            {
                tutorHand.SetActive(false);

                Control.Instance.cup.SpawnSecondTutorPack();

                Control.Instance.AddTutorialDone();

                Money.Instance.UseSoft(Control.Instance.itemsCost);

                VibrationManager.instance.VibroClick();
            }
            else
            {
                if (Control.Instance.buttonTutorialActive) return;

                if (!haveMoney)
                {
                    Money.Instance.NoMoneyShake();
                }

                bool filled = false;

                if(Control.Instance.cup.filled)
                {
                    Control.Instance.cup.NoSpaceShake();

                    filled = true;
                }

                if (!haveMoney | filled) return;

                Money.Instance.UseSoft(Control.Instance.itemsCost);

                Control.Instance.AddPackClick();

                VibrationManager.instance.VibroClick();
            }

            Refresh();
        });

        Refresh();

        Money.Instance.onMoneyChanged += Refresh;

        if(!Control.Instance.addTutorialDone)
        {
            tutorHand.SetActive(true);
        }
    }

    private IEnumerator Autoclicking()
    {
        while(true)
        {
            yield return new WaitForSeconds(autoclickRate);

            if (haveMoney & !Control.Instance.cup.filled)
            {
                Money.Instance.UseSoft(Control.Instance.itemsCost);

                Control.Instance.AddPackClick();

                VibrationManager.instance.VibroClick();

                transform.DOComplete();

                transform.DOPunchScale(Vector3.one * -.2f, .2f, 1);
            }
        }
    }

    public void Refresh()
    {
        var cost = Control.Instance.itemsCost;

        blobsCount_text.text = $"+{Control.Instance.itemsCount}";
        price_text.text = $"{cost}";

        haveMoney = Money.Instance.softValue >= cost;

        var material = new Material(bg_image.material);

        material.SetFloat(saturationProp, haveMoney ? 1 : 0);

        bg_image.material = material;
        items_image.material = material;
        money_image.material = material;

        var upgradesController = Control.Instance.upgradesController;

        if(upgradesController.autoclickUpgrade > 0)
        {
            if(upgradesController.autoclickActive)
            {
                autoclickRate = upgradesController.GetAutoclickRate();

                if(autoclickRate > 0f & autoclickRoutine == null)
                {
                    autoclickRoutine = StartCoroutine(Autoclicking());
                }
            }
            else
            {
                if(autoclickRoutine != null)
                {
                    StopCoroutine(autoclickRoutine);

                    autoclickRoutine = null;
                }
            }
        }
    }
}
