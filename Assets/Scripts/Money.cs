using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using DG.Tweening;

public class Money : MonoBehaviour
{
    public static Money Instance;

    public MoneyAdd moneyAdd_prefab;

    public RectTransform notificationsRect;

    public int softValue, hardValue;

    public TextMeshProUGUI soft_text, hard_text;

    public Action onMoneyChanged;

    private void Awake()
    {
        Instance = this;

        softValue = PlayerPrefs.GetInt("money_soft", 50);
        hardValue = PlayerPrefs.GetInt("money_hard");
    }

    private void Start()
    {
        SetText();
    }

    private void SetText()
    {
        soft_text.text = $"{Control.Instance.GetShrinkNumberString(softValue)}";
        hard_text.text = $"{this.hardValue}";

    }

    public void AddSoft(int value)
    {
        this.softValue += value;

        SetText();

        onMoneyChanged?.Invoke();

    }

    public void UseSoft(int value)
    {
        this.softValue -= value;

        if (softValue < 0) softValue = 0;

        SetText();

        onMoneyChanged?.Invoke();

        SaveState();
    }

    internal void NoMoneyShake()
    {
        transform.DOComplete();

        transform.DOShakePosition(1f, 14f);

        VibrationManager.instance.VibroNoMoney();
    }

    public void AddHard(int value)
    {
        this.hardValue += value;

        SetText();

        onMoneyChanged?.Invoke();

    }

    public void UseHard(int value)
    {
        this.hardValue -= value;

        SetText();

        onMoneyChanged?.Invoke();

        SaveState();
    }

    public void SaveState()
    {
        PlayerPrefs.SetInt("money_soft", softValue);
        PlayerPrefs.SetInt("money_hard", hardValue);
    }

    public MoneyAdd AddMoney(Vector3 worldPos)
    {
        var money = Instantiate(moneyAdd_prefab);

        money.transform.SetParent(notificationsRect);

        money.transform.localScale = Vector3.one;

        var moneyRectTransform = money.GetComponent<RectTransform>();

        var vp = Camera.main.WorldToViewportPoint(worldPos);

        var anchoredPos = new Vector3(vp.x * notificationsRect.rect.size.x, vp.y * notificationsRect.rect.size.y, 0);

        moneyRectTransform.anchoredPosition = anchoredPos;

        var pos = moneyRectTransform.localPosition;

        pos.z = 0;

        moneyRectTransform.localPosition = pos;

        return money;
    }
}
