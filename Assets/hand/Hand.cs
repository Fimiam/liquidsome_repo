using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public Animator animator;

    //public LayerMask floorMask;

    public Transform model;

    public RectTransform canvasRect;

    private bool hold;

    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var vp = Camera.main.ScreenToViewportPoint(Input.mousePosition);

        vp.x = vp.x - .5f;
        vp.y = vp.y - .5f;

        var pos = new Vector3(canvasRect.rect.size.x * vp.x, canvasRect.rect.size.y * vp.y, transform.localPosition.z);

        transform.localPosition = Vector3.Lerp(transform.localPosition, pos, .33f);

        //if(Input.GetKey(KeyCode.Alpha1))
        //{
        //    animator.SetTrigger("trein");
        //}

        //if (Input.GetKey(KeyCode.Alpha2))
        //{
        //    animator.SetTrigger("idle");
        //}

        if(Input.GetMouseButtonDown(0))
        {
            animator.SetTrigger("tap");

            hold = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            animator.SetTrigger("idle");

            hold = false;
        }

        model.localScale = Vector3.Lerp(model.localScale, hold ? Vector3.one * .88f : Vector3.one, .33f);
    }
}
